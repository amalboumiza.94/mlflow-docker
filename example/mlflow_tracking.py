
import os
from random import random, randint
from mlflow import mlflow, log_metric, log_param, log_artifacts


if __name__ == "__main__":
    # Log a parameter (key-value pair)
    mlflow.set_tracking_uri('http://localhost:5000')
    log_param("param1", randint(0, 100))

    # Log a metric; metrics can be updated throughout the run
    log_metric("foo", random())
    log_metric("foo", random() + 1)
    log_metric("foo", random() + 2)

    # Log an artifact (output file)
    if not os.path.exists("outputs"):
        os.makedirs("outputs")
    with open("outputs/test.txt", "w") as f:
        f.write("hello Phantasma Labs! Thank you for giving me this opportunity :)")
    log_artifacts("outputs")

