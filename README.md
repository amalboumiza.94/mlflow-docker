

# MLflow project with docker and kubernetes

MLFlow Docker and K8s Setup

## Requirments
### Docker : version 20.10.8
### Python : version 3.8

# Steps to run project
    1- Configure .env file for your choice. You can put there anything you like, it will be used to configure you services.
### I have chosen to work with mysql as backend and with AWS S3 service to store artifacts, which I created with terraform (check the folder create_s3bucket)

    2- Run docker-compose up -d --build
### I used docker-compose to create two containers, one for the database and the other for the MLflow tracking server using the compatible images, one of them created by myself using the Dockerfile and the miniconda image

    3- Open up http://localhost:5000 for MlFlow server and you can run test by access to the example folder and run the command 'python3 mlflow_tracking.py' to create the run under the default experiment in the mlflow server
### I choose the quickstart of the official documentation to getting start with python and to be able to test the workflow with a simple example

NOTE: I know that I put my AWS IAM user credentials in plained text in this repository and this is not secure, but I will generate a new credentials after you finish the correction of the assignement.

# Push the application into Kubernetes

## Requirements
### Kubernetes : version v1.22.3
### Minikube : version v1.24.0

## Steps to do

    1- Go under the k8s folder and run the folowing commands:
        kubectl apply -f mlflow_deployment.yaml
        kubectl apply -f mlflow_postgres.yaml

### I created these two files to create the following kubernetes components:

    * mlflow-deployment for the mlflow-server, mlflow-service to externaly access to the pod via domain name and mlflow-ingress to forward requests to that service

    * mlflow-postgres-config, the config-map for the database, the statefulSet mlflow-postgres to create the database and mlflow-postgres-service to internaly access to the database which is the default backend of the mlflow server

NOTE: I am familiar with both mysql and postgresql that's why I have choosen to use both of them as backend, one with docker and the other with kubernetes.

### You can check components using the command : kubectl get all

    2- Get the IP adress of minikube using the command :
        minikube ip and go to add the following line to the /etc/hosts file of your local machine:
        192.168.49.2    mlflow-server.local

    3- Open up http://mlflow-server.local/ for MlFlow server 

### You can also check my results by viewing the images in this repository and access to the artifacts stored in the S3 bucket using this link : 
https://mlflowstoreartifacts.s3.eu-west-3.amazonaws.com/0/95a5806ac42045959ba3dfcf514f374b/artifacts/test.txt

## Thanks for this amazing documentation:
https://www.mlflow.org/docs/latest/tracking.html

